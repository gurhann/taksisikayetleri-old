package com.kayra.taksisikayetleri.model;

import static org.junit.Assert.assertEquals;

import java.rmi.UnexpectedException;

import org.junit.Test;

public class BaseEntityTest {

	@Test(expected = IllegalArgumentException.class)
	public void whenUUIDisEmptyThenThrowException() {
		new BaseEntity("");
	}

	@Test(expected = IllegalArgumentException.class)
	public void whenUUIDisNullThenThrowException() {
		new BaseEntity(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void whenUUIDisWrongFormatThenThrowException() {
		new BaseEntity("067e6162-3b6f-4ae2-a1712-470b63dff00");
	}

	@Test
	public void whenUUIDisCorrectThenReturnCorrectUUID() throws UnexpectedException {
		BaseEntity baseEntity = new BaseEntity("067e6162-3b6f-4ae2-a171-2470b63dff00");
		assertEquals("067e6162-3b6f-4ae2-a171-2470b63dff00", baseEntity.getUUID());
	}

	@Test(expected = UnexpectedException.class)
	public void givenDefaultConstructionEntityWhenCreateUUIDThrowUnexceptedException() throws UnexpectedException {
		new BaseEntity().getUUID();
	}

}
