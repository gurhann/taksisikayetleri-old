package com.kayra.taksisikayetleri.service;

import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.kayra.taksisikayetleri.model.auth.User;
import com.kayra.taksisikayetleri.repository.UserRepository;

@Service
public class CustomUserDetailService implements UserDetailsService {

	private final UserRepository userRepository;

	@Autowired
	public CustomUserDetailService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<User> findByUser = userRepository.findByUsername(username);
		if (!findByUser.isPresent()) {
			throw new UsernameNotFoundException(String.format("User %s does not exist!", username));
		}

		User user = findByUser.get();
		UserBuilder builder = org.springframework.security.core.userdetails.User.withUsername(username);
		builder.password(findByUser.get().getPassword());
		builder.roles(user.getRoles().stream().map(role -> role.getName()).collect(Collectors.toList()).toArray(
				new String[user.getRoles().size()]));
		return builder.build();
	}

}
