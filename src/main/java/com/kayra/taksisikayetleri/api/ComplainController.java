package com.kayra.taksisikayetleri.api;

import java.net.URI;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.kayra.taksisikayetleri.constans.AppConstants;
import com.kayra.taksisikayetleri.dto.ComplainSearchDTO;
import com.kayra.taksisikayetleri.dto.UpdateComplainStatusDTO;
import com.kayra.taksisikayetleri.enums.ComplainStatus;
import com.kayra.taksisikayetleri.model.Complain;
import com.kayra.taksisikayetleri.repository.ComplainRepository;
import com.kayra.taksisikayetleri.repository.spec.ComplainSpecifications;
import com.kayra.taksisikayetleri.util.SlugUtils;

@RestController
public class ComplainController {

	@Autowired
	private ComplainRepository complainRepository;

	@RequestMapping(value = "/complains", method = RequestMethod.GET)
	public ResponseEntity<Page<Complain>> getAllComplains(Pageable pageable) {
		Page<Complain> complains = complainRepository.findByComplainStatus(ComplainStatus.APPROVED, pageable);
		return new ResponseEntity<>(complains, HttpStatus.OK);
	}

	@RequestMapping(value = "/complains/admin/search", method = RequestMethod.GET)
	public ResponseEntity<List<Complain>> searchComplain(@RequestParam(value = "status") String status,
			@RequestParam(value = "email") String email,
			@RequestParam(value = "startDate") String startDate,
			@RequestParam(value = "endDate") String endDate) {

		ComplainSearchDTO createComplainSearchDTO = createComplainSearchDTO(email, status, startDate, endDate);
		List<Complain> complains = complainRepository
				.findAll(ComplainSpecifications.customerSearcSpec(createComplainSearchDTO.getEmail(),
						createComplainSearchDTO.getStatus(),
						createComplainSearchDTO.getStartDate(),
						createComplainSearchDTO.getEndDate()));
		return new ResponseEntity<>(complains, HttpStatus.OK);
	}

	@RequestMapping(value = "/complains", method = RequestMethod.POST)
	public ResponseEntity<Complain> reportComplain(@Valid @RequestBody Complain complain) {
		complain.setUUID(UUID.randomUUID().toString());
		complain.setCreateDate(Calendar.getInstance().getTime());
		complain.setSlug(setSlug(complain.getTitle()));
		complain.setComplainStatus(ComplainStatus.WAITING_APPROVEMENT);
		complain = complainRepository.save(complain);

		HttpHeaders responseHeaders = new HttpHeaders();
		URI newComplainUri = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{slug}")
				.buildAndExpand(complain.getSlug())
				.toUri();
		responseHeaders.setLocation(newComplainUri);

		return new ResponseEntity<>(complain, responseHeaders, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/complains/{slug}", method = RequestMethod.GET)
	public ResponseEntity<?> getComplain(@PathVariable String slug) {
		Optional<Complain> findById = complainRepository.findBySlug(slug);
		return new ResponseEntity<>(findById.get(), HttpStatus.OK);
	}

	@RequestMapping(value = "/complains/admin/updateStatus", method = RequestMethod.PUT)
	public ResponseEntity<Complain> approveComplain(@RequestBody UpdateComplainStatusDTO updateStatusDTO) {
		Complain complain = complainRepository.findById(updateStatusDTO.getUuid().replaceAll("-", "")).get();
		complain.setComplainStatus(updateStatusDTO.getStatus());
		complain = complainRepository.save(complain);
		return new ResponseEntity<>(complain, HttpStatus.OK);
	}

	private String setSlug(String title) {
		String slug = SlugUtils.toSlug(title);
		Optional<Complain> findBySlug = complainRepository.findBySlug(slug);
		if (findBySlug.isPresent()) {
			slug = findBySlug.map(this::convertSlug).get();

		}
		return slug;
	}

	private String convertSlug(Complain complain) {
		String slug = complain.getSlug();
		return slug.concat("-" + new Random().nextInt(250));
	}

	private ComplainSearchDTO createComplainSearchDTO(String email, String status, String startDate, String endDate) {
		ComplainSearchDTO output = new ComplainSearchDTO();
		output.setEmail(email);
		if (!"ALL".equals(status)) {
			output.setStatus(ComplainStatus.valueOf(status));
		}
		try {
			if (StringUtils.isNotBlank(startDate)) {
				output.setStartDate(DateUtils.parseDate(startDate, AppConstants.CUSTOM_STRING_DATE_FORMAT));
			}
			if (StringUtils.isNotBlank(endDate)) {
				output.setEndDate(DateUtils.parseDate(endDate, AppConstants.CUSTOM_STRING_DATE_FORMAT));

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return output;
	}

}
