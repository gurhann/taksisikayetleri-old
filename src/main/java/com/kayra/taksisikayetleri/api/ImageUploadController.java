package com.kayra.taksisikayetleri.api;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.activation.FileTypeMap;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.ibm.icu.util.Calendar;
import com.kayra.taksisikayetleri.model.Complain;
import com.kayra.taksisikayetleri.model.ComplainImage;
import com.kayra.taksisikayetleri.repository.ComplainRepository;
import com.kayra.taksisikayetleri.repository.ImageRepository;
import com.kayra.taksisikayetleri.util.UUIDUtil;

@RestController
class ImageUploadController {

	@Value("${taksisikayetleri.upload.folder}")
	private String UPLOADED_FOLDER;

	@Autowired
	private ComplainRepository complainRepository;

	@Autowired
	ImageRepository imageRepository;

	@RequestMapping(value = "/uploadImage", method = RequestMethod.POST)
	public ResponseEntity<?> uploadFile(@RequestParam("slug") String slug,
			@RequestParam("files") MultipartFile[] uploadfiles) {

		String uploadedFileName = Arrays.stream(uploadfiles)
				.map(x -> x.getOriginalFilename())
				.filter(x -> !StringUtils.isEmpty(x))
				.collect(Collectors.joining(" , "));

		if (StringUtils.isEmpty(uploadedFileName)) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		String date = DateFormatUtils.ISO_8601_EXTENDED_DATE_FORMAT.format(Calendar.getInstance().getTime());

		try {

			checkDateFolder(date);
			String uploadPath = createFolderWithSlug(date, slug);
			saveUploadedFiles(Arrays.asList(uploadfiles), uploadPath, slug);

		} catch (IOException e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequestMapping(value = "/showImage/{imageId}", method = RequestMethod.GET)
	public ResponseEntity<byte[]> getImage(@PathVariable String imageId) throws IOException {
		Optional<ComplainImage> findById = imageRepository.findById(UUIDUtil.createBaseEntityIdFromUUID(imageId));
		File img = new File(UPLOADED_FOLDER + findById.get().getPath());
		return ResponseEntity.ok()
				.contentType(MediaType.valueOf(FileTypeMap.getDefaultFileTypeMap().getContentType(img)))
				.body(Files.readAllBytes(img.toPath()));
	}

	private void saveUploadedFiles(List<MultipartFile> files, String uploadPath, String slug) throws IOException {
		for (MultipartFile file : files) {

			if (file.isEmpty()) {
				continue; // next pls
			}

			byte[] bytes = file.getBytes();
			String imagePath = uploadPath + file.getOriginalFilename();
			Path path = Paths.get(imagePath);
			if (Files.exists(path)) {
				imagePath += new Random().nextInt(250);
			}
			path = Paths.get(imagePath);
			Files.write(path, bytes);
			insertImage(imagePath, slug);

		}

	}

	private void insertImage(String imagePath, String slug) {
		Optional<Complain> findBySlug = complainRepository.findBySlug(slug);
		findBySlug.ifPresent(complain -> {
			ComplainImage image = new ComplainImage();
			image.setPath(imagePath.replace(UPLOADED_FOLDER, ""));
			image.setUUID(UUID.randomUUID().toString());
			if (CollectionUtils.isEmpty(complain.getImageList())) {
				complain.setImageList(new ArrayList<>());
			}
			complain.getImageList().add(image);
			complainRepository.save(complain);
		});
	}

	private void checkDateFolder(String date) throws IOException {
		String dateFolderPath = new StringBuilder().append(UPLOADED_FOLDER)
				.append(date)
				.append(File.separator)
				.toString();
		Path path = Paths.get(dateFolderPath);
		if (Files.notExists(path)) {
			Files.createDirectories(path);
		}
	}

	private String createFolderWithSlug(String date, String slug) throws IOException {
		String slugFolderPath = new StringBuilder().append(UPLOADED_FOLDER)
				.append(date)
				.append(File.separator)
				.append(slug)
				.append(File.separator)
				.toString();
		Path path = Paths.get(slugFolderPath);
		Files.createDirectories(path);
		return slugFolderPath;
	}
}
