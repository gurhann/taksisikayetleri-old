package com.kayra.taksisikayetleri.util;

import java.util.HashMap;

import com.github.slugify.Slugify;

public class SlugUtils {

	private static final Slugify slg = new Slugify().withCustomReplacements(new HashMap<String, String>() {
		private static final long serialVersionUID = 1L;

		{
			put("ü", "u");
			put("ö", "o");
			put("Ü", "u");
			put("Ö", "o");
		}
	});

	private SlugUtils() {

	}

	public static String toSlug(String title) {
		return slg.slugify(title);
	}
}
