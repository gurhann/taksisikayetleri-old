package com.kayra.taksisikayetleri.util;

import org.apache.commons.lang3.StringUtils;

public class UUIDUtil {

	private UUIDUtil() {

	}

	public static void checkUUID(String uuid) {
		if (StringUtils.isEmpty(uuid)) {
			throw new IllegalArgumentException();
		}

		if (!checkCorrectUUIDFormat(uuid)) {
			throw new IllegalArgumentException();
		}
	}

	public static String createBaseEntityIdFromUUID(String uuid) {
		return uuid.replace("-", "");
	}

	private static boolean checkCorrectUUIDFormat(String uuid) {
		return uuid.length() == 36 && uuid.indexOf("-") == 8 && uuid.indexOf("-", 9) == 13
				&& uuid.indexOf("-", 14) == 18 && uuid.indexOf("-", 19) == 23;
	}

}
