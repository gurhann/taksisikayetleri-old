package com.kayra.taksisikayetleri.repository.spec;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.Predicate;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import com.kayra.taksisikayetleri.enums.ComplainStatus;
import com.kayra.taksisikayetleri.model.Complain;

public class ComplainSpecifications {

	private ComplainSpecifications() {

	}

	public static Specification<Complain> customerSearcSpec(final String email,
			final ComplainStatus status,
			final Date startDate,
			final Date endDate) {
		System.out.println(email);
		return (root, query, cb) -> {
			List<Predicate> predicates = new ArrayList<>();
			if (StringUtils.isNotEmpty(email)) {
				predicates.add(cb.equal(root.get("reporterEmail"), email));
			}

			if (status != null) {
				predicates.add(cb.equal(root.get("complainStatus"), status));
			}

			if (startDate != null && endDate != null) {
				predicates.add(cb.between(root.get("createDate"), startDate, endDate));
			}

			return cb.and(predicates.toArray(new Predicate[predicates.size()]));
		};
	}

}
