package com.kayra.taksisikayetleri.repository;

import org.springframework.data.repository.CrudRepository;

import com.kayra.taksisikayetleri.model.ComplainImage;

public interface ImageRepository extends CrudRepository<ComplainImage, String> {

}
