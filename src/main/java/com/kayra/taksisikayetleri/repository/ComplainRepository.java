package com.kayra.taksisikayetleri.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.kayra.taksisikayetleri.enums.ComplainStatus;
import com.kayra.taksisikayetleri.model.Complain;

public interface ComplainRepository
		extends PagingAndSortingRepository<Complain, String>, JpaSpecificationExecutor<Complain> {

	Optional<Complain> findBySlug(String slug);

	Page<Complain> findByComplainStatus(ComplainStatus complainStatus, Pageable peageable);
}
