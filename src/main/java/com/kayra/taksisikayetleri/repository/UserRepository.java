package com.kayra.taksisikayetleri.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.kayra.taksisikayetleri.model.auth.User;

public interface UserRepository extends CrudRepository<User, Long> {

	Optional<User> findByUsername(String username);

}