package com.kayra.taksisikayetleri.dto;

import com.kayra.taksisikayetleri.enums.ComplainStatus;

public class UpdateComplainStatusDTO {

	private String uuid;
	private ComplainStatus status;

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public ComplainStatus getStatus() {
		return status;
	}

	public void setStatus(ComplainStatus status) {
		this.status = status;
	}

}
