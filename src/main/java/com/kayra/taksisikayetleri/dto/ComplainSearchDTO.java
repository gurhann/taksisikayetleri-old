package com.kayra.taksisikayetleri.dto;

import java.util.Date;

import com.kayra.taksisikayetleri.enums.ComplainStatus;

public class ComplainSearchDTO {

	private String email;
	private Date startDate;
	private Date endDate;
	private ComplainStatus status;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public ComplainStatus getStatus() {
		return status;
	}

	public void setStatus(ComplainStatus status) {
		this.status = status;
	}

}
