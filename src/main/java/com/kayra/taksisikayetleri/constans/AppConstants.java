package com.kayra.taksisikayetleri.constans;

public interface AppConstants {

	final String APP_RESOURCE_ID = "TaksiSikayetleriAppResource";
	final String CUSTOM_STRING_DATE_FORMAT = "yyyy-MM-dd";
}
