package com.kayra.taksisikayetleri.model;

import java.rmi.UnexpectedException;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import com.kayra.taksisikayetleri.util.UUIDUtil;

@MappedSuperclass
public class BaseEntity {

	@Id
	private String id;

	public BaseEntity(String uuid) {
		setUUID(uuid);
	}

	public BaseEntity() {

	}

	public void setUUID(String uuid) {
		UUIDUtil.checkUUID(uuid);
		this.id = uuid.replaceAll("-", "");
	}

	public String getUUID() throws UnexpectedException {
		if (id == null) {
			throw new UnexpectedException("Object Id is null. Cannot create UUID");
		}
		StringBuilder sb = new StringBuilder();
		sb.append(id.substring(0, 8))
				.append("-")
				.append(id.substring(8, 12))
				.append("-")
				.append(id.substring(12, 16))
				.append("-")
				.append(id.substring(16, 20))
				.append("-")
				.append(id.substring(20, 32))
				.lastIndexOf("-");
		return sb.toString();
	}

}
