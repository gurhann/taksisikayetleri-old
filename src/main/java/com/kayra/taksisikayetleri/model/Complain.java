package com.kayra.taksisikayetleri.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.kayra.taksisikayetleri.enums.ComplainStatus;

@Entity
public class Complain extends BaseEntity {

	@NotBlank(message = "{NotBlank.complain.title}")
	@Size(max = 200, message = "{Size.complain.title}")
	private String title;

	private String slug;

	@NotBlank(message = "{NotBlank.complain.content}")
	@Column(columnDefinition = "TEXT")
	private String content;

	@NotNull(message = "{NotNull.complain.occurDate}")
	private Date occurDate;

	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@OneToMany(cascade = CascadeType.ALL)
	private List<ComplainImage> imageList;

	@NotBlank(message = "{NotBlank.complain.reporterName}")
	@Size(max = 50, message = "{Size.complain.reporterName}")
	private String reporterName;

	@NotBlank(message = "{NotBlank.complain.reporterLastname}")
	@Size(max = 50, message = "{Size.complain.reporterLastname}")
	private String reporterLastname;

	@Pattern(regexp = "^\\+905[0-9]{9}$", message = "{Pattern.complain.reporterPhone}")
	private String reporterPhone;

	@NotEmpty(message = "{NotEmpty.complain.reporterEmail}")
	@Email(message = "{Email.complain.reporterEmail}")
	private String reporterEmail;

	@Enumerated(EnumType.STRING)
	private ComplainStatus complainStatus;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getOccurDate() {
		return occurDate;
	}

	public void setOccurDate(Date occurDate) {
		this.occurDate = occurDate;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public List<ComplainImage> getImageList() {
		return imageList;
	}

	public void setImageList(List<ComplainImage> imageList) {
		this.imageList = imageList;
	}

	public String getReporterName() {
		return reporterName;
	}

	public void setReporterName(String reporterName) {
		this.reporterName = reporterName;
	}

	public String getReporterLastname() {
		return reporterLastname;
	}

	public void setReporterLastname(String reporterLastname) {
		this.reporterLastname = reporterLastname;
	}

	public String getReporterPhone() {
		return reporterPhone;
	}

	public void setReporterPhone(String reporterPhone) {
		this.reporterPhone = reporterPhone;
	}

	public String getReporterEmail() {
		return reporterEmail;
	}

	public void setReporterEmail(String reporterEmail) {
		this.reporterEmail = reporterEmail;
	}

	public ComplainStatus getComplainStatus() {
		return complainStatus;
	}

	public void setComplainStatus(ComplainStatus complainStatus) {
		this.complainStatus = complainStatus;
	}

}