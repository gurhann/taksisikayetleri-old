package com.kayra.taksisikayetleri;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaksisikayetleriApplication {

	public static void main(String[] args) {
		SpringApplication.run(TaksisikayetleriApplication.class, args);
	}

	@PostConstruct
	void started() {
		TimeZone.setDefault(TimeZone.getTimeZone("Europe/Istanbul"));
	}
}
