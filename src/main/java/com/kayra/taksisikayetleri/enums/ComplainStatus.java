package com.kayra.taksisikayetleri.enums;

public enum ComplainStatus {

	WAITING_APPROVEMENT, APPROVED, REJECTED;

}
